<?php

namespace Ass3PSS\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public function show(Request $request, $id)
    {
        $value = $request->session()->get('key');

        $value = $request->session()->get('key', 'default');

        $value = $request->session()->get('key', function () {
            return 'default';
        });
    }

    public function details(Request $request)
    {
        //prendo il valore della chiave di session
        $value = $request->session()->get('key');
        
        //se non è vuota
        if($value!=''){

            //controllo se l'utente autenticato è admin oppure no
            $isAdmin = \DB::select("select admin from users where username = '$value'");

            //se lo è
            if($isAdmin[0]->admin==1)
                //seleziono tutti i dati dell'admin
                $user = \DB::select("select * from users where admin = '1' and username = '$value'");
            else
                //seleziono tutti i dati dell'user
                $user = \DB::select("select * from users where admin = '0' and username = '$value'");
        
            //se l'utente non esiste
            if($user==[])
            {
                //ritorna la view nessun utente
                return view('nessunutente');
            }
            else
            {
                //se utente autenticato è admin
                if($isAdmin[0]->admin==1)
                    //ritorna la view dell'admin
                    return view('autenticatoadmin')->with(['user' => $user[0]]);
                else
                    //ritorna la view dell'user
                    return view('autenticato')->with(['user' => $user[0]]);
            }
        }
        else
        {
            return view('welcome');
        }

    }

    /*
    public function detailsAdmin(Request $request)
    {
        $value = $request->session()->get('key');

        $admin = \DB::select("select * from usersadmin where username = '$value'");
      
      if($admin==[]){
        return view('nessunutente');
      }else{
        return view('autenticatoadmin')
        ->with(['user' => $admin[0]]);
      }
    }*/

}
