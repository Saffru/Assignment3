<?php

namespace Ass3PSS\Http\Controllers;
use Illuminate\Http\Request;

class UtentiController extends Controller
{
    public function mostra(Request $request)
    {
        //prende la chiave di sessione
        $value = $request->session()->get('key');

        //se non è vuota
        if($value!=''){

            //controlla che l'utente sia admin ed essita
            $checkadmin = \DB::select("Select admin from users where admin = 1 and username = '" . $value . "'");
            
            if($checkadmin!=[]){
                //selesiona tutti gli utenti nel db
                $users = \DB::select("select * from users");
                if($users==[]){
                    return view('nessunutente');
                }
        
                //ritorna la view lista utenti con tutti gli utenti presenti nel db
                return view('listautenti')->with(['users' => $users]);
            }
            else{
                //ritorna la vista di azione non consentita
                return view('notaut');
            }
        }
        else{
            //ritorna la vista di azione non consentita
            return view('notaut');
        }
    }
}
