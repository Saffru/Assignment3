<?php

namespace Ass3PSS\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Exception;

class RegisterController extends Controller
{
    //Inserisce nel db un utente che si registra
    public function register(Request $request)
    {
        
        $name = $request->name;
        $username = $request->username;
        $email = $request->email;
        $password = $request->password;
        $confpassword = $request->confpassword;
        $city = $request->city;
        $country = $request->country;

        //controllo se username e psw non vuoti e psw e conferma psw sono uguali
        //(in teoria il form controlla già che non siano vuoti e non invia la richista)
        if($username!="" && $password!="" && $confpassword==$password){
            try{
                $ins = \DB::select("insert into users (username, password, name, email, city, country, admin) values('$username', '$password', '$name', '$email', '$city', '$country', 0)");
            }
            //se ci sono due utenti con stesso username da errore e faccio il catch
            catch(\Exception $e){
                print($e);
                return view('register')->with(['error' => 'duplicate']);
            }
            //metti nella sessione l'username
            Session::put('key',$username);
            //ritorna la view con tutti i dettagli utente
            return view('utenteregistrato')
            ->with(['name' => $name, 'username' => $username, 'email' => $email, 'city' => $city, 'country' => $country]);
        
        }
        else{
            //se le due psw non coincidono ritorno errore
            return view('register')->with(['error' => 'psw']);
        }
          
    }
}
