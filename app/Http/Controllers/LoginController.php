<?php

namespace Ass3PSS\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\View;
use Session;

class LoginController extends Controller
{
    private $request;

  public function login(Request $request)
  {
      $username = $request->username;
      $password = $request->password;

      //cerca l'utente nel db
      $user = \DB::select("select * from users where admin = 0 and username = '$username' and password = '$password'");
      
      //se non esiste
      if($user==[]){
        //resituisce view nessunutente
        return view('nessunutente');
      }else{
        //riempi la sessione
        Session::put('key', $username);
      
        //ritorna la view autenticato
        return view('autenticato')
        ->with(['user' => $user[0]]);
      }
  }

  public function loginadmin(Request $request)
  {
      $username = $request->username;
      $password = $request->password;

      //cerca l'admin nel db
      $admin = \DB::select("select * from users where admin = 1 and username = '$username' and password = '$password'");
      
      if($admin==[]){
        //resituisce view nessunutente
        return view('nessunutente');
      }else{
        //riempi la sessione
        Session::put('key', $username);
         
        //ritorna la view autenticat oadmin
        return view('autenticatoadmin')
        ->with(['user' => $admin[0]]);
      }
  }

}