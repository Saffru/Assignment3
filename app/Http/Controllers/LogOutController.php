<?php

namespace Ass3PSS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Session;

class LogOutController extends Controller
{
    public function logout(Request $request)
  {
    //elimina la sessione
    Session::forget('key');
    //redirect alla home
    return redirect('/');
  }
}
