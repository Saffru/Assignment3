<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/register', function () {
    return view('register')->with(['error' => '']);
});

Route::get('/loginadmin', function () {
    return view('welcomeadmin');
});

Route::get('/', function () {
    if(Session::get('key')!=""){
        return redirect('details');
    }
    else
    return view('welcome');
});

Route::get('/loginadmin', function () {
    if(Session::get('key')!=""){
        return redirect('details');
    }
    else
    return view('welcomeadmin');
});

Route::get('/register', function () {
    if(Session::get('key')!=""){
        return redirect('details');
    }
    else
        return view('register')->with(['error' => '']);
});


Route::post("/login", "LoginController@login");

Route::get("/details", "UserController@details");

Route::get('/logout', 'LogOutController@logout');

Route::get('/listautenti', 'UtentiController@mostra');

Route::get('/login',  ['uses' => 'LoginController@login', 'as' => 'login']);

Route::get('/loginadminn',  ['uses' => 'LoginController@loginadmin', 'as' => 'loginadmin']);

Route::get('/register/account',  'RegisterController@register');