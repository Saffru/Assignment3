@extends('layout.app')

<title>Home</title>

@section('content')

<div class="section"></div>
<main class="col l6 offset-l3 offset-m3 offset-s1">
	<div class="row">
		<center>
			<h5>Accedi come User</h5>
		</center>
		<div align="center">
			<form class="col s12" action="{{route('login')}}" method="get">
				<div class='row'>
					<div class='input-field col s12'>
						<input class='validate' type='text' name='username' id='username' />
						<label id="error-label" for='username' errore="" data-success="">
							<i class="material-icons">perm_identity</i> Username</label>
					</div>
				</div>
				<div class='row'>
					<div class='input-field col s12'>
						<input class='validate' type='password' name='password' id='password' />
						<label id="error-label" for='password' errore="" data-success="">
							<i class="material-icons">vpn_key</i> Password</label>
					</div>
					<label style='float: right;'>
						<a class='black-text' href='#!'>
							<b>Hai dimenticato la password?</b>
						</a>
					</label>
				</div>
				<br>
				<center>
					<div class='row'>
						<button type='submit' name='login' class='col s12 btn-large waves-effect red'>Login</button>
					</div>
					@if(Session::get('key')!="")
					<div class="card-panel green white-text lighten-2">{{"Utente '" . Session::get('key') . "' già loggato!"}}</div>
					@endif
				</center>


			</form>
		</div>
	</div>
</main>


@stop