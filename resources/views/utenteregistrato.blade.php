@extends('layout.app')

<title>Home</title>

@section('content')

<div class="section"></div>
<main class="col l6 offset-l3">
	<center>
		<h4>Utente registrato</h4>
		<h5>NAME: {{$name}}</h5>
		<h5>USERNAME: {{$username}}</h5>
		<h5>EMAIL: {{$email}}</h5>
		<h5>CITY: {{$city}}</h5>
		<h5>COUNTRY: {{$country}}</h5>
	</center>

	@if(Session::get('key')!="")
	<center>
	<div class="card-panel green white-text lighten-2">{{"Utente '" . Session::get('key') . "' registrato"}}</div>
	</center>
	@endif
</main>

@stop