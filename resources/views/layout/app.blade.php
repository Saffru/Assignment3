<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PSS ASS3</title>

	<!--Import CSS-->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/mystyle.css') }}" rel="stylesheet" type="text/css">
	<link rel="shortcut icon" href="{{{ asset('img/favicon.ico') }}}">

	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>

<body>

	<!-- Navbar -->
	<nav class="nav-fixed blue">
		<div class="nav-wrapper blue">
			<a href="/" class="brand-logo left">&emsp;Assignment 3</a>
			<ul class="right">
					@if(Session::get('key')=="")
					<li>
					<a href="/">User</a>
				</li>
				
				<li>
					<a href="/loginadmin">Admin</a>
				</li>
				<li>
					<a href="/register">Registrati</a>
				</li>
				@endif
				@if(Session::get('key')!="")
				<li>
					<a href="/details">Dettagli</a>
				</li>
				@endif
			</ul>
	</nav>


	<!-- Teal page content  -->
	<div class="col s12 m9 l9 offset-l3 offset-m3">
		<div class="row">
			<div class="col s12">@yield('content')</div>
		</div>
	</div>


	<script src="{{ asset('/js/app.js') }}"></script>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>

</body>

</html>