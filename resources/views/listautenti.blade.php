@extends('layout.app')

<title>Home</title>

@section('content')

<main class="col l6 offset-l3">
		
	<center>
			<div class="card-panel green white-text lighten-2">{{"Utente '" . Session::get('key') . "' già loggato!"}}</div>

			<h4>Lista utenti</h4>
			<table class="bordered">
				<thead>
				  <tr>
					  <th>Nome</th>
					  <th>Username</th>
					  <th>Email</th>
					  <th>City</th>
					  <th>County</th>
					  <th>Type</th>
				  </tr>
				</thead>
				<tbody>
		@foreach($users as $user)

		<tr>
			<td>{{$user->name}}</td>
			<td>{{$user->username}}</td>
			<td>{{$user->email}}</td>
			<td>{{$user->city}}</td>
			<td>{{$user->country}}</td>
			<td>{{$user->admin}}</td>
		  </tr>
		@endforeach
			</tbody>
		  </table>

		<br>
		<a href="/" class="waves-effect waves-light btn red">
			<i class="material-icons left">exit_to_app</i>Logout</a>
	</center>

</main>



@stop