@extends('layout.app')

<title>Dettagli</title>

@section('content')

<main>

	<center>
		<h4>Dettagli utente</h4>
		<h6>NAME: {{$user->name}}</h6>
		<h6>USERNAME: {{$user->username}}</h6>
		<h6>EMAIL: {{$user->email}}</h6>
		<h6>CITY: {{$user->city}}</h6>
		<h6>COUNTRY: {{$user->country}}</h6>
		<center>
			<br>

			<form class="col s12" action="{{ action('LogOutController@logout') }}" method="get">
				<a href="/logout" class="waves-effect waves-light btn red">
					<i class="material-icons left">exit_to_app</i>Logout</a>
			</form>

</main>

@endsection