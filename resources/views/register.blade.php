@extends('layout.app')

<title>Registrazione</title>

@section('content')

<main class="col l6 offset-l3">
	<center>
		@if(Session::get('key')!="")
		<div class="card-panel green white-text lighten-2">{{"Utente '" . Session::get('key') . "' già loggato!"}}</div>
		<!--<div class="col s12 m2">
				<p class="z-depth-1 green white-text lighten-2">z-depth-1</p>
			  </div>-->
		@endif

		@if($error!="")
			@if($error=="psw")
				<div class="card-panel red white-text lighten-2">Password non coincidenti</div>
			@else
				<div class="card-panel red white-text lighten-2">Nome utente già esistente</div>
			@endif

		@endif
		<h5>Registra i tuoi dati</h5>
		<img src="https://image.flaticon.com/icons/svg/61/61205.svg" alt="Smiley face" height="42" width="42">
		
		<form class="col s12" action="{{action('RegisterController@register')}}" method="get">
			<input type="hidden" name="_token" value="">
			<div class='row'>
				<div class='col s12'></div>
			</div>
			<div class='row'>
				<div class='input-field col s12'>
					<input class='validate' type='text' name='name' id='nome' />
					<label for='nome'>Nome</label>
				</div>
			</div>
			<div class='row'>
				<div class='input-field col s12'>
					<input class='validate' type='email' name='email' id='email' />
					<label for='email'>Mail</label>
				</div>
			</div>
			<div class='row'>
				<div class='input-field col s12'>
					<input class='validate' type='text' name='username' id='username' required/>
					<label for='username'>Username</label>
				</div>
			</div>
			<div class='row'>
				<div class='input-field col s12'>
					<input class='validate' type='password' name='password' id='password' required/>
					<label for='password'>Password</label>
				</div>
			</div>
			<div class='row'>
				<div class='input-field col s12'>
					<input class='validate' type='password' name='confpassword' id='confpassword' required/>
					<label for='confpassword'>Conferma password</label>
				</div>
			</div>
			<div class='row'>
				<div class='input-field col s12'>
					<input class='validate' type='text' name='city' id='citta' />
					<label for='citta'>Citta'</label>
				</div>
			</div>
			<div class='row'>
				<div class='input-field col s12'>
					<input class='validate' type='text' name='country' id='paese' />
					<label for='paese'>Paese</label>
				</div>
			</div>
			<br>
			<center>
				<div class='row'>
					<button type='submit' name='register' class='col s12 btn btn-large waves-effect red'>Registrati</button>
				</div>
			</center>

		</form>
	</center>
</main>



@stop