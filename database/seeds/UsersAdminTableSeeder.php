<?php

use Illuminate\Database\Seeder;

class UsersAdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'admin',
            'password' => 'admin',
            'name' => 'admin',
            'email' => str_random(10).'@gmail.com',
            'city' => 'Milano',
            'country' => 'Italy',
            'admin' => '1'    
        ]);

    }
}
